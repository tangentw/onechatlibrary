//
//  OneChatLibrary.swift
//  OneChatFrameWork
//
//  Created by tangentw on 21/7/2563 BE.
//  Copyright © 2563 tangentw. All rights reserved.
//

import Foundation
public class OneChatLibrary {
    private init() {}
    public static func doSomething() -> String {
        return "TG"
    }
}
